<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogApache2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_apache2', function (Blueprint $table) {
            $table->id();
            $table->string('ip');
            $table->text('identity');
            $table->string('user');
            $table->string('date');
            $table->string('time');
            $table->string('timezone');
            $table->string('method');
            $table->text('path');
            $table->string('protocol');
            $table->string('status');
            $table->string('bytes');
            $table->string('referer');
            $table->string('agent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_apache2');
    }
}
