<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogApache2 extends Model
{
    protected $table = 'log_apache2';

    public $fillable = [
        'ip',
        'identity',
        'user',
        'date',
        'time',
        'timezone',
        'method',
        'path',
        'protocol',
        'status',
        'bytes',
        'referer',
        'agent',
    ];
}
