<?php

namespace App\Http\Controllers;

use App\Models\LogApache2;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        /* /var/log/apache2/ */

        $file = file( config('apache_log.log_path') );
        $pattern = "/(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) (\".*?\") (\".*?\")/";

        //$pattern – наш шаблон
        //$line – строка для разбора
        //$result – массив, в который будут записаны полученные результаты

        foreach ($file as $string) {
            preg_match ($pattern, $string, $result);
            if ($result) {
                $model = new LogApache2();
                $model['ip'] = $result [1];
                $model['identity'] = $result [2];
                $model['user'] = $result [3];
                $model['date'] = $result [4];
                $model['time'] = $result [5];
                $model['timezone'] = $result[6];
                $model['method'] = $result [7];
                $model['path'] = $result[8];
                $model['protocol'] = $result[9];
                $model['status'] = $result[10];
                $model['bytes'] = $result[11];
                $model['referer'] = $result[12];
                $model['agent'] = $result[13];
                $model->save();
            }
        }
    }

}
